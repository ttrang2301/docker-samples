package ttrang2301.sample.docker.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ttrang2301.sample.docker.dao.UserDao;
import ttrang2301.sample.docker.model.User;

@Service
public class UserService {
    private final UserDao userDao;

    @Autowired
    public UserService (UserDao userDao) {
        this.userDao = userDao;
    }

    public User getUser (long id) {
        return this.userDao.getUser(id);
    }
}
