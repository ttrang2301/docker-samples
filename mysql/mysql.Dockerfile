FROM mysql:5.7.23

ENV MYSQL_ROOT_PASSWORD=admin
ENV MYSQL_USER=mysql
ENV MYSQL_PASSWORD=mysql
ENV MYSQL_DATABASE=docker-java

ADD initialize-schema.sql /docker-entrypoint-initdb.d/initialize-schema.sql

EXPOSE 3306